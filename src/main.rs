use cargo_toml::Manifest;
use cfg_expr::{targets::get_builtin_target_by_triple, Expression, Predicate};
use clap::Parser;
use std::fs;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    path: String,
    #[arg(short, long)]
    out: String,
}

fn strip_toml(path: &str) -> Result<String> {
    let mut aa = Manifest::from_path(path)?;
    let linux = get_builtin_target_by_triple("x86_64-unknown-linux-gnu").ok_or("no such target")?;
    aa.target.retain(|k, _| {
        Expression::parse(k).unwrap().eval(|pred| match pred {
            Predicate::Target(tp) => tp.matches(linux),
            _ => false,
        })
    });
    if let Some(ref mut prod) = aa.lib {
        if prod.proc_macro {
            prod.crate_type = vec![];
        }
    }
    let toml = toml::to_string_pretty(&aa)?;
    Ok(toml)
}

fn main() -> Result<()> {
    let args = Args::parse();
    let stripped_toml = strip_toml(&args.path)?;
    fs::write(args.out, stripped_toml)?;
    Ok(())
}
